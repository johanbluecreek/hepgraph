# HEPgraph

A little python script that via the [INSPIRE](http://inspirehep.net) API will download the information needed to create a citations-over-time graph.

## Usage

Change `author_name` variable appropriately then run:

```
$ ./hepgraph.py
```

Will generate files and place them in `~/.hepgraph/` your plots will also be saved there, under an author-subfolder.

## Example

This is the example graph for me (`author_name = blaback`)

![](plot.png)

## Known issues

* The script generates fewer cites than it should. Some of them are only implied by the INSPIRE API, and are added by the script. Others are still missing.
* Author name is hard-coded, change `author_name`.
* The script will only probe the API once. If you want a new graph, set `override` to `True`.
