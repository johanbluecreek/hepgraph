#!/usr/bin/env python3

################################################################################
        #     # ####### ######   #####  ######     #    ######  #     #
        #     # #       #     # #     # #     #   # #   #     # #     #
        #     # #       #     # #       #     #  #   #  #     # #     #
  ##### ####### #####   ######  #  #### ######  #     # ######  ####### #####
        #     # #       #       #     # #   #   ####### #       #     #
        #     # #       #       #     # #    #  #     # #       #     #
        #     # ####### #        #####  #     # #     # #       #     #
################################################################################
#
#   hepgraph.py
#   https://gitlab.com/johanbluecreek/hepgraph
#
__version__ = "0.0.1"
#
################################################################################

import requests
import json
from random import randint, uniform

import datetime as dt
import time

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import os
import pickle

import re

import warnings
warnings.filterwarnings("ignore")
from fuzzywuzzy import process, fuzz

flatten = lambda x: [ sl for l in x for sl in l]

# override

override = True

# Set author

author_name = 'blaback'

# Working directory

work_dir = os.environ['HOME'] + "/.hepgraph"

if not os.path.isdir(work_dir):
    os.mkdir(work_dir)

auth_dir = work_dir + "/" + author_name

if not os.path.isdir(auth_dir):
    os.mkdir(auth_dir)



# Get all the info we need from the referto:s

refertos = []
refertos_file = auth_dir + "/refertos.p"

if override or not os.path.isfile(refertos_file):
    r = requests.get(r'https://inspirehep.net/search',
        params={
            'of': 'recjson',
            'p': 'refersto:author:' + author_name,
            'ot': 'creation_date,recid',
            'rg': 250,
            'jrec': 0
    })

    refertos = r.json()

    i = 1
    while len(r.json()) == 250:
        i += 250
        r = requests.get(r'https://inspirehep.net/search',
        params={
            'of': 'recjson',
            'p': 'refersto:author:' + author_name,
            'ot': 'creation_date,recid',
            'rg': 250,
            'jrec': i
        })
        refertos += r.json()

    with open(refertos_file, 'wb') as f:
        pickle.dump(refertos, f)
else:
    with open(refertos_file, 'rb') as f:
        refertos = pickle.load(f)



# Get all the authors report numbers

na_re = "arXiv:[0-9]{4}.[0-9]{4,5}"
oa_re = "[a-z]{2,5}-[a-z]{2,5}/[0-9]{7}"

prnrs = []
prnrs_file = auth_dir + "/prnrs.p"

if override or not os.path.isfile(prnrs_file):
    r = requests.get(r'https://inspirehep.net/search',
        params={
            'of': 'recjson',
            'p': 'author:' + author_name,
            'ot': 'primary_report_number',
            'rg': 250,
            'jrec': 0
        })

    prnrs = r.json()

    i = 1
    while len(r.json()) == 250:
        i += 250
        r = requests.get(r'https://inspirehep.net/search',
        params={
            'of': 'recjson',
            'p': 'author:' + author_name,
            'ot': 'primary_report_number',
            'rg': 250,
            'jrec': 0
        })
        prnrs += r.json()

    prnrs = [ p['primary_report_number'] for p in prnrs ]

    prnrs = flatten(
        [ p if type(p) == list else [p] for p in prnrs ]
    )

    prnrs = [ p for p in prnrs if not p == None ]

    prnrs = sorted(
        [ p for p in prnrs if re.match(na_re, p) or re.match(oa_re, p)]
    )

    with open(prnrs_file, 'wb') as f:
        pickle.dump(prnrs, f)
else:
    with open(prnrs_file, 'rb') as f:
        prnrs = pickle.load(f)



# Get all references of the referto:s and parse out the ones of the author

alldates = []
# allcites = []
alldates_file = auth_dir + "/alldates.p"

if override or not os.path.isfile(alldates_file):

    for ref in refertos:

        # Limit the time between requests
        time.sleep(uniform(0.2,0.6))

        rlink = r'https://inspirehep.net/record/' + str(ref['recid']) + r'/references'

        r = requests.get(rlink,
            params={
                'of': 'recjson',
                'ot': 'reference'
        })

        cites_bya = [ rf['authors'] if type(rf['authors']) == list else [rf['authors']] for rf in r.json()[0]['reference'] if 'authors' in rf.keys() ]
        authors = cites_bya
        cites_bya = [process.extractOne(author_name, t, scorer=fuzz.partial_ratio) for t in cites_bya]
        cites_bya = len([ t for t in cites_bya if t[1] >= 70 ])

        cites_byr = [ rf['report_number'] for rf in r.json()[0]['reference'] if 'report_number' in rf.keys() ]
        cites_byr = len([ c for c in cites_byr if c in prnrs ])

        cites = 0
        if cites_bya > cites_byr:
            cites = cites_bya
        else:
            cites = cites_byr

        # allcites += [cites]

        # Some of these are zero for some reason, which they should not... so we cheat.
        if cites == 0:
            # print("HEPgraph: recID: " + str(ref['recid']) + "; gave no cites.")
            # print("HEPgraph: Is your name in any of these author lists:")
            #for a in authors:
            #    print(a)
            cites = 1

        cdate = dt.datetime.strptime(ref['creation_date'][0:10] ,'%Y-%m-%d').date()

        for c in range(cites):
            alldates += [cdate]

    with open(alldates_file, 'wb') as f:
        pickle.dump(alldates, f)
else:
    with open(alldates_file, 'rb') as f:
        alldates = pickle.load(f)


print("HEPgraph: I managed to find " + str(len(alldates)) + " cites in your name.")

# Plot!

x = sorted(alldates)
y = range(len(x))

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))

locator = mdates.YearLocator()
#locator.MAXTICKS = 5000

plt.gca().xaxis.set_major_locator(locator)
plt.plot(x,y)
plt.gcf().autofmt_xdate()

plt.savefig(auth_dir + "/plot" + dt.date.today().isoformat() + "-" + str(int(time.time())) + ".png" ,dpi=300)
